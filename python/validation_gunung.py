import MySQLdb as mdb
import sys
from math import sin, cos, sqrt, atan2, radians
from urllib2 import urlopen
import requests
import json


#### CRUD MySQL ####
def retrieveTable(con):
    counter=[]
    with con:
        cur = con.cursor(mdb.cursors.DictCursor)
        cur.execute("SELECT * FROM laporan_gunung_meletus JOIN user WHERE laporan_gunung_meletus.username = user.username")

        rows = cur.fetchall()
        for row in rows:
            counter.append(0)
	
	return rows, counter

def deleteRecord(con):
    with con:
        cur = con.cursor(mdb.cursors.DictCursor)
        cur.execute("DELETE FROM laporan_gunung_meletus")

        print 'tabel laporan gunung meletus sudah didelete' 


def updateRow(con, username, new_bobot):
    with con:

        cur = con.cursor()

        cur.execute("UPDATE user SET bobot = %s WHERE username = %s",
            (new_bobot, username))



# def getCentroid(arr):
#     length = arr.shape[0]
#     sum_x = np.sum(arr[:, 0])
#     sum_y = np.sum(arr[:, 1])
#     return sum_x/length, sum_y/length


def countDistance(lat1, lon1, lat2, lon2):
    R = 6373.0

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    return distance

def getCounter(array, radius, counter):

    for i in range (len(array)):
        for j in range (i, len(array)): 
            if (countDistance(float(array[i]['latitude']),  float(array[i]['longitude']), float(array[j]['latitude']), float(array[j]['longitude']))<=radius):
                
                if i==j:
                    if array[i]['username']=="admin":
                        counter[i] += 0.5*threshold_laporan_besar
                    else:
                        counter[i] += array[j]['bobot']
                else:
                    if array[i]['username']=="admin":
                        counter[i] += array[j]['bobot']
                        counter[j] += 0.5*threshold_laporan_besar
                    elif array[j]['username']=="admin":
                        counter[i] += 0.5*threshold_laporan_besar
                        counter[j] += array[i]['bobot']
                    else:
                        counter[i] += array[j]['bobot']
                        counter[j] += array[i]['bobot']

                
    return counter

def is_any_threshold_or_up(array, threshold):
    for i in array:
        if int(i) >= threshold:
            
            return True
    return False        




# SET UP THE CONNECTION
def getData():
    try:
        con = mdb.connect('localhost', 'root', '', 'ews');

        cur = con.cursor()
        cur.execute("SELECT VERSION()")


        # # CRUD OPERATIONS
        data, array_counter = retrieveTable(con)
    	
    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        sys.exit(1)

    finally:
        if con:
            con.close()

    return data, array_counter

def deleteTable():
    try:
        con = mdb.connect('localhost', 'root', '', 'ews');

        cur = con.cursor()
        cur.execute("SELECT VERSION()")


        # # CRUD OPERATIONS
        deleteRecord(con)
        
    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        sys.exit(1)

    finally:
        if con:
            con.close()

def updateBobot(username, new_bobot):
    try:
        con = mdb.connect('localhost', 'root', '', 'ews');

        cur = con.cursor()
        cur.execute("SELECT VERSION()")


        # # CRUD OPERATIONS
        updateRow(con,username, new_bobot)
        
    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        sys.exit(1)

    finally:
        if con:
            con.close()





############### MAIN #####################
import time


threshold_laporan_kecil=3 #threshold petugas
threshold_laporan_besar=10 #threshod keseluruhan
threshold_waktu = 1000 #detik
radius_gunung =5.0 #dalam kilometer

URL ="http://172.20.10.4/ews/pushnotification.php" #tehering


while True:
    photos=[]
    data, init_counter = getData()
    while len(data)==0:
        print 'data kosong'
        data, init_counter = getData()
        time.sleep(5)

    starttime=time.time()

    array_counter=getCounter(data, radius_gunung, init_counter)
    differtime = time.time() - starttime
    
    while (differtime < threshold_waktu) and (is_any_threshold_or_up(array_counter,threshold_laporan_kecil)==False):
        #print 'waktu: ', differtime
        data, init_counter = getData()
        array_counter=getCounter(data, radius_gunung, init_counter)
        
        differtime = time.time() - starttime + 1
        print array_counter
        time.sleep(5)
    
    ########## Send ke Petugas ########## 
    if differtime >= threshold_waktu:
        print 'differtime > ', threshold_waktu
        deleteTable()
    elif is_any_threshold_or_up(array_counter, threshold_laporan_kecil):
        print array_counter
        print'valid for petugas'

        array_lat=[]
        array_long=[]
        for i in range (len(data)):
            if array_counter[i]>=threshold_laporan_kecil:
                array_lat.append(float(data[i]['latitude'])) 
                array_long.append(float(data[i]['longitude']))
                if (data[i]['image_path'] != ''):
                    photos.append("http://172.20.10.4/ews/"+data[i]['image_path'])
                

        

        lat_average = sum(array_lat)/len(array_lat)
        long_average = sum(array_long)/len(array_long)
        lat_average=str(lat_average)
        long_average = str(long_average)
        rad=str(radius_gunung)
        

        data = {'lat':lat_average,
                'long':long_average,
                'radius':rad,
                'jenis':'gunung',
                'user':'admin',
                'photos':','.join(photos)
               }
        

        print data
        r = requests.get(URL, data)
        print 'URL : ', r.url
        print 'Sending Notification to petugas'
        print 'RESPONSE : ', r.text 

        #urlopen(url)
        data, init_counter = getData()
        array_counter=getCounter(data, radius_gunung, init_counter)



    while (differtime < threshold_waktu) and (is_any_threshold_or_up(array_counter,threshold_laporan_besar)==False):
        differtime = time.time() - starttime + 1
        #print 'waktu: ', differtime
        
        data, init_counter = getData()
        array_counter=getCounter(data, radius_gunung, init_counter)
        print array_counter
        time.sleep(5)

    ########## Send ke semua #####
    if differtime >= threshold_waktu:
        print 'differtime > 60'
        for i in range(len(data)):
            new_bobot = data[i]['bobot'] - data[i]['bobot']*0.1
            updateBobot(data[i]['username'],new_bobot)
        deleteTable()
    elif is_any_threshold_or_up(array_counter, threshold_laporan_besar):
        print'valid for all'

        array_lat=[]
        array_long=[]

        for i in range (len(data)):
            if array_counter[i]>=threshold_laporan_besar:
                array_lat.append(float(data[i]['latitude'])) 
                array_long.append(float(data[i]['longitude']))
                new_bobot = data[i]['bobot'] + data[i]['bobot']*0.1
                updateBobot(data[i]['username'], new_bobot)
            else :
                new_bobot = data[i]['bobot'] - data[i]['bobot']*0.1
                updateBobot(data[i]['username'], new_bobot)
        

        lat_average = sum(array_lat)/len(array_lat)
        long_average = sum(array_long)/len(array_long)
        lat_average=str(lat_average)
        long_average = str(long_average)
        rad=str(radius_gunung)

        
          
        #tethering iphone
        url = "http://172.20.10.4/ews/pushnotification.php/?lat=" + lat_average + "&long=" + long_average + "&radius=" + rad + "&jenis=gunung&user=all"
        
        urlopen(url)
        deleteTable()
        time.sleep(10)




    time.sleep(5) #tunggu 5 detik

