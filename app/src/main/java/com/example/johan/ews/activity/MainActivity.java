package com.example.johan.ews.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.johan.ews.R;
import com.example.johan.ews.Session;
import com.example.johan.ews.app.Config;
import com.example.johan.ews.util.NotificationUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends Activity {

    private TextView gempaBtn, banjirBtn, tsunamiBtn, gunungBtn;
    //public Button gempaBtn, banjirBtn, tsunamiBtn, gunungBtn;
    private TextView txtRegId, txtMessage;
    private Button logoutBtn, takePhoto;
    private ImageView imageToUpload;
    private android.content.BroadcastReceiver mRegistrationBroadcastReceiver;
    private Session session;
    private Bitmap bitmap;

    String bmp ="";

    LocationManager locationManager;
    LocationListener locationListener;

    RequestQueue requestQueue;
    String URL="";

    StringRequest request;
    double longitude, latitude;
    private final String TAG = "main ";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setUserTypeOnButtonClick();
        URL = getString(R.string.URL) + "createreport.php";

        session = new Session(this);
        txtMessage = (TextView) findViewById(R.id.txt_push_message);
        txtMessage.setText("Welcome " + session.getUsername());
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("Tess", refreshedToken);

        latitude = session.getLat();
        longitude = session.getLong();


        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("Android","Android ID : "+android_id);
        FirebaseMessaging.getInstance().subscribeToTopic("ewsjohan")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = getString(R.string.msg_subscribed);
                        if (!task.isSuccessful()) {
                            msg = getString(R.string.msg_subscribe_failed);
                        }
                        Log.d(TAG, msg);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                Log.d("xx", intent.getStringExtra("message"));

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    //displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    String jenis = intent.getStringExtra("jenis");
                    String[] photos_array = intent.getStringArrayExtra("photos");
                    String untuk = intent.getStringExtra("untuk");

                    Toast.makeText(getApplicationContext(), "WARNING: " + message, Toast.LENGTH_LONG).show();

                    if (session.getUsername().equals("admin")){
                        Log.e("MainActivity","username admin");
                        if(untuk=="admin"){
                            Log.e("MainActivity","notif untuk admin");
                            Intent photos = new Intent(MainActivity.this, PhotosActivity.class);
                            photos.putExtra("jenis", jenis);
                            photos.putExtra("photos", photos_array);
                            startActivity(photos);
                        } else{
                            Log.e("MainActivity","notif untuk all");
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                            alertDialog.setMessage(message);
                            alertDialog.setPositiveButton("Ok",  new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                            alertDialog.show();
                        }

                    } else {
                        Log.e("MainActivity","Username masyarakat");
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                        alertDialog.setMessage(message);
                        alertDialog.setPositiveButton("Ok",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.show();
                    }



                    //txtMessage.setText(message);
                }
            }
        };
        //displayFirebaseRegId();


        /*gempaBtn = (Button) findViewById(R.id.gempa_btn);
        banjirBtn = (Button) findViewById(R.id.banjir_btn);
        tsunamiBtn = (Button) findViewById(R.id.tsunami_btn);
        gunungBtn = (Button) findViewById(R.id.gunung_btn);*/

        gempaBtn = (TextView) findViewById(R.id.gempa_btn);
        banjirBtn = (TextView) findViewById(R.id.banjir_btn);
        tsunamiBtn = (TextView) findViewById(R.id.tsunami_btn);
        gunungBtn = (TextView) findViewById(R.id.gunung_btn);
        logoutBtn = (Button) findViewById(R.id.btnLogout);
        takePhoto = (Button) findViewById(R.id.take_photo);
        imageToUpload = (ImageView) findViewById(R.id.imageToUpload);

        requestQueue = Volley.newRequestQueue(this);


        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);
            }
        });

        gempaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bitmap != null){
                    bmp = imageToString(bitmap);
                }
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                alertDialog.setMessage("Apakah anda yakin ingin membuat laporan?");
                alertDialog.setPositiveButton("Ya",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        request = new StringRequest(Request.Method.POST, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.names().get(0).equals("success")) {
                                                gempaBtn.setClickable(false);
                                                Log.d("debuggerrr", "success");
                                                Toast.makeText(getApplicationContext(), "SUCCESS " + jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                                imageToUpload.setImageResource(0);
                                                imageToUpload.setVisibility(View.GONE);
                                            } else {
                                                Log.d("debuugg", "error");
                                                Toast.makeText(getApplicationContext(), "Error" + jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            Log.d("debugg", "exception");
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> parameters = new HashMap<String, String>();
                                parameters.put("latitude", String.valueOf(latitude));
                                parameters.put("longitude", String.valueOf(longitude));
                                parameters.put("jenis", "gempa");
                                parameters.put("username",session.getUsername());
                                parameters.put("image",bmp);

                                return parameters;
                            }
                        };
                        requestQueue.add(request);

                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();

            }
        });


        banjirBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                alertDialog.setMessage("Apakah anda yakin ingin membuat laporan?");
                alertDialog.setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        request = new StringRequest(Request.Method.POST, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.names().get(0).equals("success")) {
                                                banjirBtn.setClickable(false);
                                                Log.d("debuggerrr", "success");
                                                Toast.makeText(getApplicationContext(), "SUCCESS " + jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                                imageToUpload.setImageResource(0);
                                                imageToUpload.setVisibility(View.GONE);
                                            } else {
                                                Log.d("debuugg", "error");
                                                Toast.makeText(getApplicationContext(), "Error" + jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> parameters = new HashMap<String, String>();
                                parameters.put("latitude", String.valueOf(latitude));
                                parameters.put("longitude", String.valueOf(longitude));
                                parameters.put("jenis", "banjir");
                                parameters.put("username",session.getUsername());
                                parameters.put("image",bmp);

                                return parameters;
                            }
                        };
                        requestQueue.add(request);

                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();


            }
        });


        tsunamiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                alertDialog.setMessage("Apakah anda yakin ingin membuat laporan?");
                alertDialog.setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        request = new StringRequest(Request.Method.POST, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.names().get(0).equals("success")) {
                                                Log.d("debuggerrr", "success");
                                                tsunamiBtn.setClickable(false);
                                                Toast.makeText(getApplicationContext(), "SUCCESS " + jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                                imageToUpload.setImageResource(0);
                                                imageToUpload.setVisibility(View.GONE);
                                            } else {
                                                Log.d("debuugg", "error");
                                                Toast.makeText(getApplicationContext(), "Error" + jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> parameters = new HashMap<String, String>();
                                parameters.put("latitude", String.valueOf(latitude));
                                parameters.put("longitude", String.valueOf(longitude));
                                parameters.put("jenis", "tsunami");
                                parameters.put("username",session.getUsername());
                                parameters.put("image",bmp);

                                return parameters;
                            }
                        };
                        requestQueue.add(request);

                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();


            }
        });

        gunungBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                alertDialog.setMessage("Apakah anda yakin ingin membuat laporan?");
                alertDialog.setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        request = new StringRequest(Request.Method.POST, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.names().get(0).equals("success")) {
                                                gunungBtn.setClickable(false);
                                                Log.d("debuggerrr", "success");
                                                Toast.makeText(getApplicationContext(), "SUCCESS " + jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                                imageToUpload.setImageResource(0);
                                                imageToUpload.setVisibility(View.GONE);
                                            } else {
                                                Log.d("debuugg", "error");
                                                Toast.makeText(getApplicationContext(), "Error" + jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> parameters = new HashMap<String, String>();
                                parameters.put("latitude", String.valueOf(latitude));
                                parameters.put("longitude", String.valueOf(longitude));
                                parameters.put("jenis", "gunung");
                                parameters.put("username",session.getUsername());
                                parameters.put("image",bmp);

                                return parameters;
                            }
                        };
                        requestQueue.add(request);

                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();


            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setLoggedin(false);
                session.setUsername("");
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                finish();

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode==1){
//            Uri selectedImage = data.getData();
//            imageToUpload.setImageURI(selectedImage);
            Bundle bundle = data.getExtras();
            bitmap = (Bitmap) bundle.get("data");
            imageToUpload.setImageBitmap(bitmap);
        }
    }


    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(imgBytes,Base64.DEFAULT);
    }

}
