package com.example.johan.ews.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.johan.ews.R;
import com.example.johan.ews.Session;
import com.example.johan.ews.ViewPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PhotosActivity extends Activity {
    private ImageView image;
    private TextView TV;
    private Button YesButton, NoButton;
    ViewPager viewpager;
    ViewPagerAdapter adapter;
    private double latitude, longitude;
    private Session session;
    String bmp ="";

    RequestQueue requestQueue;
    String URL="";

    StringRequest request;

    private String[] images ={
            "http://172.20.10.4/ews/photos/13818_1533948154.jpeg",
            "http://172.20.10.4/ews/photos/9797_1533547184.jpeg",
            "https://cdn.sindonews.net/dyn/620/content/2017/08/28/42/1234433/banjir-bandang-landa-houston-bandara-george-bush-lumpuh-hV3.jpg",
            "http://www.suarapemredkalbar.com/as-assets/images/news/IMG_20170815_75cdbe8e30_1502797143.jpg"

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        YesButton = (Button) findViewById(R.id.btnYes);
        NoButton = (Button) findViewById(R.id.btnNo);
        //TV = (TextView) findViewById(R.id.txt);
        //image = (ImageView) findViewById(R.id.image_recieved);


        requestQueue = Volley.newRequestQueue(this);
        URL = getString(R.string.URL) + "createreport.php";
        session = new Session(this);

        Intent intentExtras = getIntent();
        String msg = intentExtras.getStringExtra("message");
        final String jenis = intentExtras.getStringExtra("jenis");
        final String[] photos = intentExtras.getStringArrayExtra("photos");

        adapter = new ViewPagerAdapter(PhotosActivity.this, photos);
        viewpager = (ViewPager)findViewById(R.id.ViewPager);

        viewpager.setAdapter(adapter);

        latitude = session.getLat();
        longitude = session.getLong();

        YesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("latitude", String.valueOf(latitude));
                Log.e("longitude", String.valueOf(longitude));
                Log.e("jenis", jenis);
                Log.e("username",session.getUsername());
                Log.e("image",bmp);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PhotosActivity.this);

                alertDialog.setMessage("Apakah anda yakin ingin membuat laporan?");
                alertDialog.setPositiveButton("Ya",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code


                        request = new StringRequest(Request.Method.POST, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.names().get(0).equals("success")) {
                                                YesButton.setClickable(false);
                                                Log.d("debuggerrr", "success");
                                                Toast.makeText(getApplicationContext(), "SUCCESS " + jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                                startActivity(new Intent(PhotosActivity.this,MainActivity.class));
                                                YesButton.setClickable(true);

                                            } else {
                                                Log.d("debuugg", "error");
                                                Toast.makeText(getApplicationContext(), "Error" + jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            Log.d("debugg", "exception");
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> parameters = new HashMap<String, String>();
                                parameters.put("latitude", String.valueOf(latitude));
                                parameters.put("longitude", String.valueOf(longitude));
                                parameters.put("jenis", jenis);
                                parameters.put("username",session.getUsername());
                                parameters.put("image",bmp);

                                return parameters;
                            }
                        };
                        requestQueue.add(request);

                    }
                });
                alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();

            }
        });

        NoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(PhotosActivity.this,MainActivity.class));
            }
        });

    }
}
