package com.example.johan.ews.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.johan.ews.R;
import com.example.johan.ews.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends Activity implements View.OnClickListener{
    private Button reg;
    private TextView backLogin;
    private EditText etusername, etpassword;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        session = new Session(this);

        reg = (Button) findViewById(R.id.btnReg);
        backLogin = (TextView) findViewById(R.id.BackToLogin);
        etusername = (EditText) findViewById(R.id.etUsername);
        etpassword = (EditText) findViewById(R.id.etPassword);
        reg.setOnClickListener(this );
        backLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnReg:
                regist();
                break;
            case R.id.BackToLogin:
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                finish();
                break;
            default:
        }
    }

    private void regist() {
        final String username = etusername.getText().toString();
        final String pass = etpassword.getText().toString();
        final String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        String URLregist = getString(R.string.URL) + "/regist.php";

        if (!username.isEmpty() && !pass.isEmpty()) {
            StringRequest request = new StringRequest(Request.Method.POST, URLregist,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.names().get(0).equals("success")) {
                                    Log.d("regiisstt", "success");
                                    session.setLoggedin(true);
                                    session.setUsername(username);
                                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                    finish();
                                    Toast.makeText(getApplicationContext(), "SUCCESS " + jsonObject.getString("success"), Toast.LENGTH_LONG).show();
                                } else {
                                    Log.d("regiisstt", "error");
                                    Toast.makeText(getApplicationContext(), "Error" + jsonObject.getString("error"), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("username", String.valueOf(username));
                    parameters.put("password", String.valueOf(pass));
                    parameters.put("id", android_id);

                    return parameters;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(request);

        } else{
            etusername.setError("Please insert username");
            etpassword.setError("Please insert password");
        }
    }
}
