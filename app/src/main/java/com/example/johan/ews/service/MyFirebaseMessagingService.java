package com.example.johan.ews.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.johan.ews.R;
import com.example.johan.ews.Session;
import com.example.johan.ews.activity.LoginActivity;
import com.example.johan.ews.activity.MainActivity;
import com.example.johan.ews.activity.PhotosActivity;
import com.example.johan.ews.app.Config;
import com.example.johan.ews.util.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService{
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private Session session;
    double curLat, curLong, centerLat, centerLong, rad, dist;

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override



    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        session = new Session(this);
        curLat = (double)session.getLat();
        curLong = (double)session.getLong();

        if (remoteMessage == null)
            return;

        //Check if device in a radius to get notif
        //if (dist < rad){

            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                Log.d("tess", "masuk a");
                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                handleNotification(remoteMessage.getNotification().getBody());
                //Log.e(TAG, "Notification Body: " + bodyMSG.getString("msg"));
                //handleNotification(bodyMSG.getString("msg"));
                //handleNotification(remoteMessage.getData().get("body"));
            }

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.d("tess", "masuk b");
                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

                try {
                    JSONObject json = new JSONObject(remoteMessage.getData().toString());
                    handleDataMessage(json);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }

        //}

    }

    private void handleNotification(String message) {

        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Log.d("tess", "masuk app in FG");
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
            Log.d("tess", "masuk app in BG");
        }

    }

    private void handleDataMessage(JSONObject json) {

        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            String timestamp = data.getString("timestamp");
            String user = data.getString("user");

            JSONArray photos = data.getJSONArray("photos");
            String[] photos_array=new String[photos.length()];
            for(int i=0; i<photos_array.length; i++) {
                photos_array[i]=photos.optString(i);
            }


            JSONObject payload = data.getJSONObject("payload");
            String jenis = payload.getString("jenis_bencana");
            centerLat = Double.parseDouble(payload.getString("lat"));
            centerLong = Double.parseDouble(payload.getString("long"));
            rad = Double.parseDouble(payload.getString("rad"));


            dist =distance(centerLat, centerLong,curLat,curLong);

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "photos: " + photos_array);

            if (dist <= rad) {
                if (session.getUsername().equals("admin")){
                    //Login sebagai admin
                    if (user.equals("admin")){ //ditujukan utk admin
                        Log.e("FB","notif untuk admin");
                        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                            // app is in foreground, broadcast the push message
                            Log.d("tess", "app in FG");
                            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            pushNotification.putExtra("jenis",jenis);
                            pushNotification.putExtra("photos", photos_array);
                            pushNotification.putExtra("untuk","admin");

                            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                            // play notification sound
                            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                            notificationUtils.playNotificationSound();
                        } else {
                            // app is in background, show the notification in notification tray
                            Log.d("tess", "app in bG");
                            Intent resultIntent = new Intent(getApplicationContext(), PhotosActivity.class);
                            resultIntent.putExtra("message", message);
                            resultIntent.putExtra("jenis",jenis);
                            resultIntent.putExtra("photos", photos_array);


                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);

                        }

                    } else{ //buat masyarakat
                        Log.e("FB","notif untuk masyarakat");
                        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                            // app is in foreground, broadcast the push message
                            Log.d("tess", "app in FG");
                            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            pushNotification.putExtra("jenis",jenis);
                            pushNotification.putExtra("photos", photos_array);
                            pushNotification.putExtra("untuk","all");
                            //pushNotification.putExtra("photos",photos);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                            // play notification sound
                            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                            notificationUtils.playNotificationSound();
                        } else {
                            // app is in background, show the notification in notification tray
                            Log.d("tess", "app in bG");
                            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                            resultIntent.putExtra("message", message);
                            resultIntent.putExtra("untuk","all");

                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);

                        }
                    }
                    Log.e("CEK USER", "masuk ke " + session.getUsername());


                } else {
                    //login sebagai masyarakat
                    Log.e("CEK USER", user + " bukan admin");
                    if (user.equals("all")){

                        Log.e("CEK USER", user + "masyarakat");
                        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                            // app is in foreground, broadcast the push message
                            Log.d("tess", "app in FG");
                            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                            pushNotification.putExtra("message", message);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                            // play notification sound
                            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                            notificationUtils.playNotificationSound();
                        } else {
                            // app is in background, show the notification in notification tray
                            Log.d("tess", "app in bG");
                            Intent resultIntent;
                            if (session.getUsername()!= ""){
                                resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                                resultIntent.putExtra("message", message);
                            } else{
                                resultIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                resultIntent.putExtra("message", message);
                            }

                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);

                        }
                    } else if (user.equals("admin")){
                        Log.e("CEK USER", "hanya untuk admin, tapi yang terima masy");
                    }

                }

            }

        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {

        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        String URL = getString(R.string.URL) + "/photos/";
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, URL);
    }

    /*private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {

        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }*/

}
