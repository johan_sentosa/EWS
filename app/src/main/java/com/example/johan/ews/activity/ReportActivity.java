package com.example.johan.ews.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.johan.ews.R;

import java.util.List;

public class ReportActivity extends AppCompatActivity {

    private final int N_COLUMN = 2;
    private Button mTakePhoto;
    private ImageView imgPhoto;
    private RecyclerView listPhoto;
    private ReportAdapter reportAdapter;

    private GridLayoutManager gridLayoutManager;

    private List<String> listPhotoURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        mTakePhoto = (Button) findViewById(R.id.btn_take_photo);
        imgPhoto = (ImageView) findViewById(R.id.img_report_photo);
        listPhoto = (RecyclerView) findViewById(R.id.list_photo_report);

        gridLayoutManager = new GridLayoutManager(this, N_COLUMN);

        // TODO: 8/5/2018 insert the url list into the list
        reportAdapter = new ReportAdapter(listPhotoURL, this);

        listPhoto.setLayoutManager(gridLayoutManager);
        listPhoto.setAdapter(reportAdapter);

    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                // TODO: 8/6/2018 do something in this case log out 
                break;
        }
        return true;
    }

    class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportHolder> {

        private List<String> listImage;
        private Activity activity;

        public ReportAdapter(List<String> listImage, Activity activity) {
            this.listImage = listImage;
            this.activity = activity;
        }

        @NonNull
        @Override
        public ReportHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new ReportHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_report, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(ReportHolder holder, int i) {

            Glide.with(activity).load(listImage.get(i)).into(holder.image);

        }

        @Override
        public int getItemCount() {
            return 0;
        }


        class ReportHolder extends RecyclerView.ViewHolder {

            public ImageView image;

            public ReportHolder(View view) {
                super(view);
                image = (ImageView) findViewById(R.id.image);
            }
        }
    }
}
