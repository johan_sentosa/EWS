package com.example.johan.ews;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context ctx;

    public Session(Context ctx){
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences("MyApp",Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public void setLoggedin(boolean status){
        editor.putBoolean("loggedInmode",status);
        editor.commit();
    }

    public boolean loggedin(){
        return prefs.getBoolean("loggedInmode", false);
    }

    public void setUsername(String uname){
        editor.putString("username",uname);
        editor.commit();
    }

    public String getUsername() { return prefs.getString("username",""); }

    public void setLat(float latitude){
        editor.putFloat("latitude", latitude);
        editor.commit();
    }

    public void setLong(float longitude){
        editor.putFloat("longitude", longitude);
        editor.commit();
    }

    public float getLat() {return prefs.getFloat("latitude", (float)0.0); }
    public float getLong() {return prefs.getFloat("longitude", (float)0.0); }
}
